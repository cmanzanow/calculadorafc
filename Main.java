import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    //System.out.println("Hello world!");
    CALCULADORA calc = new CALCULADORA();
    boolean continuar = true;
    Scanner scan = new Scanner(System.in);
    double a;
    double b;
    System.out.println("Bienvenido a la Calculadora");
    while(continuar){
      System.out.println("Menu");
      System.out.println("(1) Sumar");
      System.out.println("(2) Restar");
      System.out.println("(3) Multiplicar");
      System.out.println("(4) Dividir");
      System.out.println("(5) Elevar");
      System.out.println("(6) Raiz Cuadrada");
      System.out.println("(7) Raiz N");
      System.out.println("(8) Logaritmo Natural");
      System.out.println("(9) Logaritmo Base 10");
      System.out.println("(10) Procentaje");
      System.out.println("(10) Salir");
      System.out.print("Seleccione una opción: ");
      int opcion = scan.nextInt();

      if(opcion == 1){
          System.out.print("Ingrese número 1: ");
          a = scan.nextDouble();
          System.out.print("Ingrese número 2: ");
          b = scan.nextDouble();
          calc.Sumar(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 2) {
          System.out.print("Ingrese número 1: ");
          a = scan.nextDouble();
          System.out.print("Ingrese número 2: ");
          b = scan.nextDouble();
          calc.Restar(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 3) {
          System.out.print("Ingrese número 1: ");
          a = scan.nextDouble();
          System.out.print("Ingrese número 2: ");
          b = scan.nextDouble();
          calc.MULTIPLICAR(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 4) {
          System.out.print("Ingrese número 1: ");
          a = scan.nextDouble();
          System.out.print("Ingrese número 2: ");
          b = scan.nextDouble();
          if(b == 0){
            System.out.println("No se puede dividir por 0");
            while(b==0){
              System.out.print("Ingrese número 2: ");
              b = scan.nextDouble();
              if(b == 0){
                System.out.println("No se puede dividir por 0");
              }
            }
          }
          calc.DIVIDIR(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 5) {
          System.out.print("Ingrese Base: ");
          a = scan.nextDouble();
          System.out.print("Ingrese Exponente: ");
          b = scan.nextDouble();
          calc.Elevar(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 6) {
          System.out.print("Ingrese número: ");
          a = scan.nextDouble();
          calc.RaizCuadrada(a);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 7) {
          System.out.print("Ingrese número: ");
          a = scan.nextDouble();
          System.out.print("Ingrese raíz: ");
          b = scan.nextDouble();
          calc.RaizN(a,b);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 8) {
          System.out.print("Ingrese número: ");
          a = scan.nextDouble();
          calc.LogN(a);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 9) {
          System.out.print("Ingrese número: ");
          a = scan.nextDouble();
          calc.Log10(a);
          System.out.println("Resultado: "+ calc.Total);
        }else if (opcion == 10) {
          System.out.print("Ingrese número 1: ");
          a = scan.nextDouble();
          System.out.print("Ingrese número 2: ");
          b = scan.nextDouble();
          if(b == 0){
            System.out.println("El segundo numero no puede ser 0");
            while(b==0){
              System.out.print("Ingrese número 2: ");
              b = scan.nextDouble();
              if(b == 0){
                System.out.println("El segundo numero no puede ser 0");
              }
            }
          }
          calc.Porcentaje(a,b);
          System.out.println("Resultado: "+ calc.Total+"%");
        }else if (opcion == 11) {
          continuar = false;
        }

      }



  }
}
