import java.util.Scanner;

/**
 *
 * @author Mateus
 */
public class EXPONENTE {
    Scanner lectura = new Scanner(System.in);
    
    String n;
    String n2;
    String total;
    
    public EXPONENTE(String n, String n2, String total){
        this.n=n;
        this.n2=n2;
        this.total=total;
    }
    
    EXPONENTE(){
        
    }
    
    public String getN(){
        return n;
    }
    
    public String getN2(){
        return n2;
    }
    
    public String getTotal(){
        return total;
    }
    
    public void setN(String n){
        this.n=n;
    }
    
    public void setN2(String n2){
        this.n2=n2;
    }
    
    public void total(String total){
        this.total=total;
    }
    
    public double EXPONENTE_OBTENER(String n, String n2){
        double resultado;
        System.out.println("\nTOTAL: " + Math.pow(Double.parseDouble(n), Double.parseDouble(n2)) + "\n");
        resultado = Math.pow(Double.parseDouble(n), Double.parseDouble(n2));
        return resultado;
    }
    
}
