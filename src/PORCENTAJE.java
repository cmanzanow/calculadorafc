import java.util.Scanner;

/**
 *
 * @author Mateus
 */
public class PORCENTAJE{
    Scanner lectura = new Scanner(System.in);
    
    String n;
    String n2;
    
    public PORCENTAJE(String n, String n2){
        this.n=n;
        this.n2=n2;
    }
    
    PORCENTAJE(){
        
    }
    
    public String getN(){
        return n;
    }
    
    public String getN2(){
        return n2;
    }
 
    public void setN(String n){
        this.n=n;
    }
    
    public void setN2(String n2){
        this.n2=n2;
    }
    
    public double PORCENTAJE_OBTENER(String n, String n2){
        double resultado, C;
        C = (Double.parseDouble(n)*100);
        
        resultado = C /Double.parseDouble(n2);
        return resultado;
    }
    
}
