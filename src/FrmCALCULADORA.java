
import javax.swing.JOptionPane;

public class FrmCALCULADORA extends javax.swing.JFrame {
    
    String memoria1;
    String signo;
    String memoria2;
    
    public FrmCALCULADORA() {
        initComponents();
        this.setLocationRelativeTo(this); //centrar Programa en la pantalla. //
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        Botón9 = new javax.swing.JButton();
        Botón6 = new javax.swing.JButton();
        BotónResta = new javax.swing.JButton();
        BotónDivisión = new javax.swing.JButton();
        BotónMultiplicar = new javax.swing.JButton();
        BotónSuma = new javax.swing.JButton();
        BotónDEL = new javax.swing.JButton();
        BotónResultado = new javax.swing.JButton();
        BotónAC = new javax.swing.JButton();
        BotónPI = new javax.swing.JButton();
        BotónEuler = new javax.swing.JButton();
        BotónRaízEnécima = new javax.swing.JButton();
        txtpantalla = new javax.swing.JTextField();
        BotónRaízCuadrada = new javax.swing.JButton();
        Botón1 = new javax.swing.JButton();
        BotónExponente = new javax.swing.JButton();
        Botón4 = new javax.swing.JButton();
        BotónParéntesisIzquierdo = new javax.swing.JButton();
        Botón7 = new javax.swing.JButton();
        BotónParéntesisDerecho = new javax.swing.JButton();
        Botón0 = new javax.swing.JButton();
        BotónLOG = new javax.swing.JButton();
        Botón2 = new javax.swing.JButton();
        Botón8 = new javax.swing.JButton();
        Botón5 = new javax.swing.JButton();
        Botón3 = new javax.swing.JButton();
        BotónPunto = new javax.swing.JButton();
        BotónDivisión1 = new javax.swing.JButton();
        BotónPorcentaje_de_num = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        jToolBar1.setRollover(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Botón9.setBackground(new java.awt.Color(51, 51, 51));
        Botón9.setForeground(new java.awt.Color(255, 255, 255));
        Botón9.setText("9");
        Botón9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón9ActionPerformed(evt);
            }
        });

        Botón6.setBackground(new java.awt.Color(51, 51, 51));
        Botón6.setForeground(new java.awt.Color(255, 255, 255));
        Botón6.setText("6");
        Botón6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón6ActionPerformed(evt);
            }
        });

        BotónResta.setBackground(new java.awt.Color(51, 51, 51));
        BotónResta.setForeground(new java.awt.Color(255, 255, 255));
        BotónResta.setText("-");
        BotónResta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRestaActionPerformed(evt);
            }
        });

        BotónDivisión.setBackground(new java.awt.Color(51, 51, 51));
        BotónDivisión.setForeground(new java.awt.Color(255, 255, 255));
        BotónDivisión.setText("/");
        BotónDivisión.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDivisiónActionPerformed(evt);
            }
        });

        BotónMultiplicar.setBackground(new java.awt.Color(51, 51, 51));
        BotónMultiplicar.setForeground(new java.awt.Color(255, 255, 255));
        BotónMultiplicar.setText("*");
        BotónMultiplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónMultiplicarActionPerformed(evt);
            }
        });

        BotónSuma.setBackground(new java.awt.Color(51, 51, 51));
        BotónSuma.setForeground(new java.awt.Color(255, 255, 255));
        BotónSuma.setText("+");
        BotónSuma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónSumaActionPerformed(evt);
            }
        });

        BotónDEL.setBackground(new java.awt.Color(153, 51, 0));
        BotónDEL.setForeground(new java.awt.Color(255, 255, 255));
        BotónDEL.setText("Del");
        BotónDEL.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDELActionPerformed(evt);
            }
        });

        BotónResultado.setBackground(new java.awt.Color(51, 51, 51));
        BotónResultado.setForeground(new java.awt.Color(255, 255, 255));
        BotónResultado.setText("=");
        BotónResultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónResultadoActionPerformed(evt);
            }
        });

        BotónAC.setBackground(new java.awt.Color(153, 51, 0));
        BotónAC.setForeground(new java.awt.Color(255, 255, 255));
        BotónAC.setText("AC");
        BotónAC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónACActionPerformed(evt);
            }
        });

        BotónPI.setBackground(new java.awt.Color(51, 51, 51));
        BotónPI.setForeground(new java.awt.Color(255, 255, 255));
        BotónPI.setText("PI");
        BotónPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPIActionPerformed(evt);
            }
        });

        BotónEuler.setBackground(new java.awt.Color(51, 51, 51));
        BotónEuler.setForeground(new java.awt.Color(255, 255, 255));
        BotónEuler.setText("e");
        BotónEuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónEulerActionPerformed(evt);
            }
        });

        BotónRaízEnécima.setBackground(new java.awt.Color(51, 51, 51));
        BotónRaízEnécima.setForeground(new java.awt.Color(255, 255, 255));
        BotónRaízEnécima.setText("x√");
        BotónRaízEnécima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRaízEnécimaActionPerformed(evt);
            }
        });

        BotónRaízCuadrada.setBackground(new java.awt.Color(51, 51, 51));
        BotónRaízCuadrada.setForeground(new java.awt.Color(255, 255, 255));
        BotónRaízCuadrada.setText("√");
        BotónRaízCuadrada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónRaízCuadradaActionPerformed(evt);
            }
        });

        Botón1.setBackground(new java.awt.Color(51, 51, 51));
        Botón1.setForeground(new java.awt.Color(255, 255, 255));
        Botón1.setText("1");
        Botón1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón1ActionPerformed(evt);
            }
        });

        BotónExponente.setBackground(new java.awt.Color(51, 51, 51));
        BotónExponente.setForeground(new java.awt.Color(255, 255, 255));
        BotónExponente.setText("^");
        BotónExponente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónExponenteActionPerformed(evt);
            }
        });

        Botón4.setBackground(new java.awt.Color(51, 51, 51));
        Botón4.setForeground(new java.awt.Color(255, 255, 255));
        Botón4.setText("4");
        Botón4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón4ActionPerformed(evt);
            }
        });

        BotónParéntesisIzquierdo.setBackground(new java.awt.Color(51, 51, 51));
        BotónParéntesisIzquierdo.setForeground(new java.awt.Color(255, 255, 255));
        BotónParéntesisIzquierdo.setText("(");
        BotónParéntesisIzquierdo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónParéntesisIzquierdoActionPerformed(evt);
            }
        });

        Botón7.setBackground(new java.awt.Color(51, 51, 51));
        Botón7.setForeground(new java.awt.Color(255, 255, 255));
        Botón7.setText("7");
        Botón7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón7ActionPerformed(evt);
            }
        });

        BotónParéntesisDerecho.setBackground(new java.awt.Color(51, 51, 51));
        BotónParéntesisDerecho.setForeground(new java.awt.Color(255, 255, 255));
        BotónParéntesisDerecho.setText(")");
        BotónParéntesisDerecho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónParéntesisDerechoActionPerformed(evt);
            }
        });

        Botón0.setBackground(new java.awt.Color(51, 51, 51));
        Botón0.setForeground(new java.awt.Color(255, 255, 255));
        Botón0.setText("0");
        Botón0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón0ActionPerformed(evt);
            }
        });

        BotónLOG.setBackground(new java.awt.Color(51, 51, 51));
        BotónLOG.setForeground(new java.awt.Color(255, 255, 255));
        BotónLOG.setText("Log");
        BotónLOG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónLOGActionPerformed(evt);
            }
        });

        Botón2.setBackground(new java.awt.Color(51, 51, 51));
        Botón2.setForeground(new java.awt.Color(255, 255, 255));
        Botón2.setText("2");
        Botón2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón2ActionPerformed(evt);
            }
        });

        Botón8.setBackground(new java.awt.Color(51, 51, 51));
        Botón8.setForeground(new java.awt.Color(255, 255, 255));
        Botón8.setText("8");
        Botón8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón8ActionPerformed(evt);
            }
        });

        Botón5.setBackground(new java.awt.Color(51, 51, 51));
        Botón5.setForeground(new java.awt.Color(255, 255, 255));
        Botón5.setText("5");
        Botón5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón5ActionPerformed(evt);
            }
        });

        Botón3.setBackground(new java.awt.Color(51, 51, 51));
        Botón3.setForeground(new java.awt.Color(255, 255, 255));
        Botón3.setText("3");
        Botón3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Botón3ActionPerformed(evt);
            }
        });

        BotónPunto.setBackground(new java.awt.Color(51, 51, 51));
        BotónPunto.setForeground(new java.awt.Color(255, 255, 255));
        BotónPunto.setText(".");
        BotónPunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPuntoActionPerformed(evt);
            }
        });

        BotónDivisión1.setBackground(new java.awt.Color(51, 51, 51));
        BotónDivisión1.setForeground(new java.awt.Color(255, 255, 255));
        BotónDivisión1.setText("a/b");
        BotónDivisión1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónDivisión1ActionPerformed(evt);
            }
        });

        BotónPorcentaje_de_num.setBackground(new java.awt.Color(51, 51, 51));
        BotónPorcentaje_de_num.setForeground(new java.awt.Color(255, 255, 255));
        BotónPorcentaje_de_num.setText("%");
        BotónPorcentaje_de_num.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotónPorcentaje_de_numActionPerformed(evt);
            }
        });

        jMenu1.setText("Opciones");

        jMenu3.setText("Convertidor");

        jMenuItem2.setText("Volumen");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        jMenuItem3.setText("Longitud");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        jMenuItem4.setText("Temperatura");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem5.setText("Peso");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuItem6.setText("Presion");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem6);

        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(Botón4, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón5, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón6, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónMultiplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónDivisión, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Botón0, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónPI, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(BotónEuler, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(Botón1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónSuma, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónResta, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Botón7, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón8, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Botón9, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónLOG, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónExponente, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(txtpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 332, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BotónRaízEnécima, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónRaízCuadrada, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónParéntesisIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónParéntesisDerecho, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BotónPorcentaje_de_num, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(BotónDivisión1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BotónAC, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BotónDEL, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtpantalla, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotónAC, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónDivisión1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónPorcentaje_de_num, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotónRaízEnécima, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónRaízCuadrada, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónParéntesisIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónParéntesisDerecho, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónDEL, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón7, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón8, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón9, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónLOG, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónExponente, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónMultiplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónDivisión, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Botón1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónSuma, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónResta, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BotónPunto, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónPI, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BotónEuler, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Botón0, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Botón9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón9ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"9");
    }//GEN-LAST:event_Botón9ActionPerformed

    private void Botón6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón6ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"6");
    }//GEN-LAST:event_Botón6ActionPerformed

    private void BotónRestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónRestaActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="-";
            txtpantalla.setText("");
        }

    }//GEN-LAST:event_BotónRestaActionPerformed

    private void BotónDivisiónActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónDivisiónActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="/";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónDivisiónActionPerformed

    private void BotónMultiplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónMultiplicarActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="*";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónMultiplicarActionPerformed

    private void BotónSumaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónSumaActionPerformed
        // TODO add your handling code here
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="+";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónSumaActionPerformed

    private void BotónDELActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónDELActionPerformed
        // TODO add your handling code here:
        String cadena;
        cadena=txtpantalla.getText();

        if (cadena.length()>0) {
            cadena=cadena.substring(0, cadena.length()-1);
            txtpantalla.setText(cadena);
        }
    }//GEN-LAST:event_BotónDELActionPerformed

    private void BotónResultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónResultadoActionPerformed
        // TODO add your handling code here:
        String resultado;
        memoria2=txtpantalla.getText();

        if (!memoria2.equals("")) {
            resultado=calculadora(memoria1,memoria2,signo);
            txtpantalla.setText(resultado);
        }

    }//GEN-LAST:event_BotónResultadoActionPerformed
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
    
    public static String calculadora(String memoria1,String memoria2,String signo){
    Double resultado=0.0;
    String respuesta;
    
    
    if (signo.equals("-")) {
        
        RESTAR resta = new RESTAR();
        
        resta.setN(memoria1);
        resta.setN2(memoria2);
                    
        resultado = resta.RESTAR_OBTENER(memoria1,memoria2);
        
    }
    if (signo.equals("+")) {
        
        SUMAR suma = new SUMAR();
        
        suma.setN(memoria1);
        suma.setN2(memoria2);
                    
        resultado = suma.SUMAR_OBTENER(memoria1, memoria2);
        
    }
    if (signo.equals("*")) {
        
        MULTIPLICAR multi = new MULTIPLICAR();
        
        multi.setN(memoria1);
        multi.setN2(memoria2);
        
        resultado = multi.MULTIPLICAR_OBTENER(memoria1,memoria2);
                
    }
    if (signo.equals("/")) {
        
        DIVIDIR div = new DIVIDIR();
        
        div.setN(memoria1);
        div.setN2(memoria2);
        
        resultado = div.DIVIDIR_OBTENER(memoria1,memoria2);
                
    }
    
    if (signo.equals("[")) {
        
        RAIZ raiz = new RAIZ();
        
        raiz.setN(memoria1);
                
        resultado = raiz.RAIZ_OBTENER(memoria1);
                
    }
    
    if (signo.equals("^")){
        
        EXPONENTE exponente = new EXPONENTE();
        
        exponente.setN(memoria1);
        exponente.setN2(memoria2);
        
        resultado = exponente.EXPONENTE_OBTENER(memoria1, memoria2);
        
    }
    
    if (signo.equals("°")) { 
        
        RAIZENÉSIMA enésima = new RAIZENÉSIMA();
        
        enésima.setN(memoria1);
        enésima.setN2(memoria2);
                
        resultado = enésima.RAIZENÉSIMA_OBTENER(memoria1, memoria2);
    }
    
    if (signo.equals("%")) { 
        
        PORCENTAJE porcentaje = new PORCENTAJE();
        
        porcentaje.setN(memoria1);
        porcentaje.setN2(memoria2);
                
        resultado = porcentaje.PORCENTAJE_OBTENER(memoria1, memoria2);
    }
    

    
    respuesta=resultado.toString();
    return respuesta;
}
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
    private void BotónACActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónACActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText("");
    }//GEN-LAST:event_BotónACActionPerformed

    private void BotónPIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónPIActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="PI";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónPIActionPerformed

    private void BotónEulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónEulerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BotónEulerActionPerformed

    private void BotónRaízEnécimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónRaízEnécimaActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="°";
            txtpantalla.setText("");
        }
        
    }//GEN-LAST:event_BotónRaízEnécimaActionPerformed

    private void BotónRaízCuadradaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónRaízCuadradaActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="[";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónRaízCuadradaActionPerformed

    private void Botón1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón1ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"1");
    }//GEN-LAST:event_Botón1ActionPerformed

    private void BotónExponenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónExponenteActionPerformed
        // TODO add your handling code here:
         if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="^";
            txtpantalla.setText("");
        }
    }//GEN-LAST:event_BotónExponenteActionPerformed

    private void Botón4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón4ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"4");
    }//GEN-LAST:event_Botón4ActionPerformed

    private void BotónParéntesisIzquierdoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónParéntesisIzquierdoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BotónParéntesisIzquierdoActionPerformed

    private void Botón7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón7ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"7");
    }//GEN-LAST:event_Botón7ActionPerformed

    private void BotónParéntesisDerechoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónParéntesisDerechoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BotónParéntesisDerechoActionPerformed

    private void Botón0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón0ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"0");
    }//GEN-LAST:event_Botón0ActionPerformed

    private void BotónLOGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónLOGActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BotónLOGActionPerformed

    private void Botón2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón2ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"2");
    }//GEN-LAST:event_Botón2ActionPerformed

    private void Botón8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón8ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"8");
    }//GEN-LAST:event_Botón8ActionPerformed

    private void Botón5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón5ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"5");
    }//GEN-LAST:event_Botón5ActionPerformed

    private void Botón3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Botón3ActionPerformed
        // TODO add your handling code here:
        txtpantalla.setText(txtpantalla.getText()+"3");
    }//GEN-LAST:event_Botón3ActionPerformed

    private void BotónPuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónPuntoActionPerformed
        // TODO add your handling code here:
        String cadena;
        cadena=txtpantalla.getText();

        if (cadena.length()<=0) {
            txtpantalla.setText("0.");

        }
        else{
            if (!existepunto(txtpantalla.getText())) {
                txtpantalla.setText(txtpantalla.getText()+".");

            }
        }

    }//GEN-LAST:event_BotónPuntoActionPerformed

    private void BotónDivisión1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónDivisión1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BotónDivisión1ActionPerformed

    private void BotónPorcentaje_de_numActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotónPorcentaje_de_numActionPerformed
        // TODO add your handling code here:
        if (!txtpantalla.getText().equals("")) {
            memoria1=txtpantalla.getText();
            signo="%";
            txtpantalla.setText("");
        }
        
    }//GEN-LAST:event_BotónPorcentaje_de_numActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
              new Volumen().setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        new Longitud().setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        new Temperatura().setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        new Peso().setVisible(true);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new Presion().setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    public static boolean existepunto(String cadena){
        boolean resultado;
        resultado=false;
        
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.substring(i, i+1).equals(".")) {
                resultado=true;
                break;
                
                
                
                
            }
            
        }
        return resultado;
        
                
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmCALCULADORA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new FrmCALCULADORA().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Botón0;
    private javax.swing.JButton Botón1;
    private javax.swing.JButton Botón2;
    private javax.swing.JButton Botón3;
    private javax.swing.JButton Botón4;
    private javax.swing.JButton Botón5;
    private javax.swing.JButton Botón6;
    private javax.swing.JButton Botón7;
    private javax.swing.JButton Botón8;
    private javax.swing.JButton Botón9;
    private javax.swing.JButton BotónAC;
    private javax.swing.JButton BotónDEL;
    private javax.swing.JButton BotónDivisión;
    private javax.swing.JButton BotónDivisión1;
    private javax.swing.JButton BotónEuler;
    private javax.swing.JButton BotónExponente;
    private javax.swing.JButton BotónLOG;
    private javax.swing.JButton BotónMultiplicar;
    private javax.swing.JButton BotónPI;
    private javax.swing.JButton BotónParéntesisDerecho;
    private javax.swing.JButton BotónParéntesisIzquierdo;
    private javax.swing.JButton BotónPorcentaje_de_num;
    private javax.swing.JButton BotónPunto;
    private javax.swing.JButton BotónRaízCuadrada;
    private javax.swing.JButton BotónRaízEnécima;
    private javax.swing.JButton BotónResta;
    private javax.swing.JButton BotónResultado;
    private javax.swing.JButton BotónSuma;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JTextField txtpantalla;
    // End of variables declaration//GEN-END:variables
}
