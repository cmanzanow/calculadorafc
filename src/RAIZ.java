
import java.util.Scanner;

public class RAIZ {
    
    Scanner lectura = new Scanner(System.in);
    
    String n;

    public RAIZ(String n)
    {
        this.n=n;

    }

    RAIZ() {
        
    }
    
    public String getN()
    {
        return n;
    }
    
    public void setN(String n)
    {
        this.n=n;
    }
    
    public double RAIZ_OBTENER(String n){
        double resultado;
        resultado = (double)Math.sqrt(Double.parseDouble(n));
        return resultado;
    }
    
}
